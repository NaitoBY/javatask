import database.Faculty;
import database.Teacher;
import services.FacultyServices;
import java.util.List;

public class Main {


    private static FacultyServices facultyService = new FacultyServices();
    public static Faculty faculty = new Faculty("физра","330-20-29","Фролов И.В.");

    public void addFaculty(){
        facultyService.saveFaculty(faculty);
        Teacher teacher = new Teacher("Фролов И.В.");
        teacher.setFaculty(faculty);
        faculty.addTeacher(teacher);
        facultyService.updateFaculty(faculty);

    }

    public void outputFaculty(){
        List<Faculty> facultyList = facultyService.findAllFaculty();
        for (Faculty facultyOut : facultyList){
            System.out.println(facultyOut);
        }
    }

    public void updateFaculty(){
        faculty.setNamefaculty("Биология");
        facultyService.updateFaculty(faculty);
    }

    public void deleteFaculty(){
        facultyService.deleteFaculty(faculty);
    }
    public static void main(String[] args){
        Main main = new Main();

        main.addFaculty();
        main.outputFaculty();
        main.updateFaculty();
        main.outputFaculty();
        main.deleteFaculty();
        main.outputFaculty();

    }

}
