package database;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "faculty")
public class Faculty {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String namefaculty;
    private String phone;
    private String manager;
    @OneToMany(mappedBy = "faculty", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Teacher> teachers;
    public Faculty(){
    }
    public Faculty(String namefaculty, String phone,String manager) {
        this.namefaculty = namefaculty;
        this.phone = phone;
        this.manager = manager;
        teachers = new ArrayList<>();
    }

    public void addTeacher(Teacher teacher) {
        teacher.setFaculty(this);
        teachers.add(teacher);
    }

    public int getId(){
        return id;
    }

    public String getNamefaculty(){
        return  namefaculty;
    }
    public void setNamefaculty(String namefaculty) {
        this.namefaculty = namefaculty;
    }

    public  String getPhone(){
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getManager(){
        return manager;
    }
    public void setManager(String manager) {
        this.manager = manager;
    }

    public  List<Teacher> getTeachers(){
        return teachers;
    }
    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    @Override
    public String toString() {
        return "database.faculty{" +
                "id=" + id +
                ", namefaculty='" + namefaculty +
                ", phone='" + phone +
                ", manager=" + manager +
                '}';
    }

}
