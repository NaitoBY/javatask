package database;

import javax.persistence.*;

@Entity
@Table(name = "head_of_department")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String fio;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_faculty")
    private Faculty faculty;

    public Teacher(){

    }
    public Teacher(String fio){
        this.fio = fio;
    }

    public int getId(){
        return id;
    }

    public String getFio(){
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Faculty getFaculty(){
        return faculty;
    }
    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return fio+" ";
    }
}
