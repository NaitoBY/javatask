package services;

import dao.FacultyDao;
import database.Teacher;
import database.Faculty;

import java.util.List;

public class FacultyServices {

    private FacultyDao facultyDao = new FacultyDao();


    public Faculty findFaculty(int id) {
        return facultyDao.findById(id);
    }

    public void saveFaculty(Faculty faculty) {
        facultyDao.save(faculty);
    }

    public void deleteFaculty(Faculty faculty) {
        facultyDao.delete(faculty);
    }

    public void updateFaculty(Faculty faculty) {
        facultyDao.update(faculty);
    }

    public List<Faculty> findAllFaculty() {
        return facultyDao.findAll();
    }

    public Teacher findTeacherById(int id) {
        return facultyDao.findTeacherById(id);
    }

}
