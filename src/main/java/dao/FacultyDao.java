package dao;

import database.Faculty;
import database.Teacher;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;
import java.util.List;

public class FacultyDao {

    public Faculty findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Faculty.class, id);
    }

    public void save(Faculty faculty) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(faculty);
        tx1.commit();
        session.close();
    }

    public void update(Faculty faculty) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(faculty);
        tx1.commit();
        session.close();
    }

    public void delete(Faculty faculty) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(faculty);
        tx1.commit();
        session.close();
    }

    public Teacher findTeacherById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Teacher.class, id);
    }

    public List<Faculty> findAll() {
        List<Faculty> faculties = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Faculty").list();
        return faculties;
    }
}
