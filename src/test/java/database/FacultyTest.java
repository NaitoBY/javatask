package database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.*;
import org.junit.jupiter.api.Assertions;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

import static org.junit.Assert.*;

public class FacultyTest {
    private static SessionFactory sessionFactory;
    private Session session;

    @BeforeClass
    public static void setup() {
        sessionFactory = HibernateSessionFactoryUtil.getSessionFactory();
        System.out.println("SessionFactory created");
    }

    @AfterClass
    public static void tearDown() {
        if (sessionFactory != null) sessionFactory.close();
        System.out.println("SessionFactory destroyed");
    }
    @Test
    public void addFaculty() {
    session.beginTransaction();

    Faculty faculty = new Faculty("физра","330-20-29","Фролов И.В.");
    Integer id = (Integer) session.save(faculty);
        Teacher teacher = new Teacher("Фролов И.В.");
        teacher.setFaculty(faculty);
        faculty.addTeacher(teacher);
        session.update(faculty);
    session.getTransaction().commit();
    Assertions.assertTrue(id > 0);
    }
    @Test
    public void getFaculty() {

        Integer id = 5;
        Faculty faculty = session.find(Faculty.class, id);
        assertEquals("Истории", faculty.getNamefaculty());
    }
    @Test
    public void listFaculty() {
        Query<Faculty> query = session.createQuery("from Faculty", Faculty.class);
        List<Faculty> resultList = query.getResultList();
        Assertions.assertFalse(resultList.isEmpty());
    }
    @Test
    public void deleteFaculty() {
        int id = 6;
        Faculty deletedFaculty = session.find(Faculty.class, id);
        Assertions.assertNull(deletedFaculty);
    }

    @Before
    public void openSession() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    @After
    public void closeSession() {
        if (session != null) session.close();
        System.out.println("Session closed\n");
    }
}