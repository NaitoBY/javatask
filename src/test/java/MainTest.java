import database.Faculty;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import services.FacultyServices;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    private static FacultyServices facultyService = new FacultyServices();

    private Faculty faculty;
    private Faculty faculty1;
    private Faculty faculty2;

    @Before
    public void setUp() throws Exception {
         faculty = new Faculty("физра","330-20-29","Фролов И.В.");
         faculty1 = new Faculty("химия","220-20-29","Киров И.В.");
         faculty2 = new Faculty("физика","110-20-29","Шарапов И.В.");
    }


    @Test
    public void outputAllFaculty() {
        List<Faculty> expected = facultyService.findAllFaculty();
        List<Faculty> actual = new ArrayList<>();
        actual.add(faculty);
        actual.add(faculty1);
        actual.add(faculty2);
        Assert.assertEquals(expected, actual);

    }
    @Test
    public void getOutputFaculty_NO_NULL() {
        List<Faculty> expected = facultyService.findAllFaculty();
        Assert.assertNotNull(expected);
    }



}